package com.makiewicz.model;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;


@RunWith(Parameterized.class)
public class RestDataTest {

    @Parameterized.Parameter(value = 0)
    public String id;

    @Parameterized.Parameter(value = 1)
    public int size;

    @Parameterized.Parameter(value = 2)
    public TreeSet<Integer> setOfNumbers;

    @Parameterized.Parameter(value = 3)
    public TreeSet<Integer> expectedResult;

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider(){
        return Arrays.asList(new Object[][]{
                {"1", 7, new TreeSet<>(Arrays.asList(23,2,7,8,144,2,2)),new TreeSet<>(Arrays.asList(2, 7,  23))},
                {"2", 6, new TreeSet<>(Arrays.asList(2,4,4,577,577,88)),new TreeSet<>(Arrays.asList(2,577))},
                {"3", 14, new TreeSet<>(Arrays.asList(994,577,577,5,162,5,653,179,16,181,22,37,37,41)),
                        new TreeSet<>(Arrays.asList(5,37,41,179,181,577,653))},
                {"4", 0, new TreeSet<>(Arrays.asList()),new TreeSet<>(Arrays.asList())},
                {"5", 3, new TreeSet<>(Arrays.asList(4,4,88)),new TreeSet<>(Arrays.asList())}
        });
    }



    private RestData restDataObject;

    @Before
    public void setup(){
        restDataObject = new RestData(id,size,setOfNumbers);
    }

    @Test
    public void leavePrimeNumbersRemoveDuplicatesAndSort(){
        restDataObject.leaveOnlyPrimeNumbers();
        Assert.assertEquals(expectedResult,restDataObject.getSetOfNumbers());
        //Assert.assertEquals(1,1);
    }

}
