package com.makiewicz.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.makiewicz.dao.RestDao;
import com.makiewicz.model.RestData;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.IOException;


@Controller
public class MyController {

    @RequestMapping(value = "/getNumbers", method = RequestMethod.GET)
    public String getPrimeNumbers(ModelMap model){
        String restDataInJson = "";
        try {
            restDataInJson = RestDao.getRestDataFromUrlAdress(
                    "http://dt-gwitczak-recruitment.westeurope.cloudapp.azure.com:8080/rest/task",
                    "candidate",
                    "abc123");
        }catch (IOException e){
            e.printStackTrace();
        }

        RestData restData = new RestData();
        ObjectMapper mapper = new ObjectMapper();
        try {
            restData = mapper.readValue(restDataInJson, RestData.class);
        }catch (IOException e) {
            e.printStackTrace();
        }
        restData.leaveOnlyPrimeNumbers();

        model.addAttribute("primeNumbers", restData.getSetOfNumbers());
        return "PrimeNumbers";
    }

}
