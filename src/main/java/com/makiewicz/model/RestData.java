package com.makiewicz.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;


public class RestData {

    @JsonProperty("id")
    private String id;

    @JsonProperty("size")
    private int size;

    @JsonProperty("data")
    private TreeSet<Integer> setOfNumbers = new TreeSet<>();

    public RestData() {
    }

    public RestData(String id, int size, TreeSet<Integer> setOfNumbers) {
        this.id = id;
        this.size = size;
        this.setOfNumbers = setOfNumbers;
    }

    public String getId() {
        return id;
    }

    public int getSize() {
        return size;
    }

    public Set<Integer> getSetOfNumbers() {
        return setOfNumbers;
    }

    @Override
    public String toString() {
        return "RestData{" +
                "id='" + id + '\'' +
                ", size=" + size +
                ", setOfNumbers=" + setOfNumbers +
                '}';
    }


    public void leaveOnlyPrimeNumbers(){
        Iterator<Integer> iter = setOfNumbers.iterator();

        while(iter.hasNext()){
            Integer i = iter.next();
            if(!isPrime(i)){
                iter.remove();
            }
        }
    }

    public static boolean isPrime (int number){
        //checking if number is even
        if(number > 2 && ((number%2)==0)) {
            return false;
        }
        //checking odd numbers up to number^0,5
        for(int i = 3; i * i <= number; i += 2){
            if (number % i == 0)
                return false;
        }
        return true;
    }
}
