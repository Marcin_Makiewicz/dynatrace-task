package com.makiewicz.dao;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import java.io.IOException;

public class RestDao {

    private static Logger LOG = Logger.getLogger(RestDao.class);

    public static String getRestDataFromUrlAdress(String urlAdress, String username, String password) throws IOException{
        LOG.debug("Creating httpClient");
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username,password);
        provider.setCredentials(AuthScope.ANY, credentials);

        HttpClient httpClient = HttpClientBuilder.create()
                .setDefaultCredentialsProvider(provider)
                .build();


        LOG.debug("Creating http request to: " + urlAdress );
        HttpGet get = new HttpGet(urlAdress);
        HttpResponse response = httpClient.execute(get);
        int statusCode = response.getStatusLine().getStatusCode();

        if(statusCode >= 200 && statusCode < 300){
            LOG.debug("Success response from: " + urlAdress);
            return EntityUtils.toString(response.getEntity());
        } else {
            throw new RuntimeException("Bad response code: " + statusCode);
        }
    }
}
