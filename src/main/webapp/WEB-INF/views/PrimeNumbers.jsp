<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Prime Numbers</title>

    <%--bootstrap--%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>

<body>
    <div style="margin: 10px">
        <button class="btn btn-outline-dark" style="margin-bottom: 20px" onclick="location.href='/getNumbers'">Pobierz nowe dane</button>
        <table class="table">
            <th>Liczby pierwsze:</th>
            <c:forEach items="${primeNumbers}" var="item">
                <tr>
                    <td>
                        <c:out value="${item}"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
